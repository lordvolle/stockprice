﻿
using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;
using StockPricesApp.Models;

namespace StockPricesApp
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Graph", // Route name
                "history", // URL with parameters
                new {controller = "Graph", action = "Index", id = UrlParameter.Optional} // Parameter defaults
                );

            routes.MapRoute(
                "Default", // Route name
                "{controller}/{action}/{id}", // URL with parameters
                new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
            );
        }

        private Thread _fillThread;
        private Thread _getThread;
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            RegisterGlobalFilters(GlobalFilters.Filters);
            RegisterRoutes(RouteTable.Routes);
            Global.CurrentValues = new List<Price>();
            _fillThread = new Thread(DbFiller.Fill);
            _fillThread.Start();
            _getThread = new Thread(DbGetter.GetValues);
            _getThread.Start();

        }



    }
}