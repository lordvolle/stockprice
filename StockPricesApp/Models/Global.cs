﻿using System.Collections.Generic;

namespace StockPricesApp.Models
{
    public class Global
    {
        private static List<Price> _historyVal;

        private static int size = 90; //Time*metals 30 sec * 3; 180 = 1 min for 3 metals...


        public static List<Price> CurrentValues { get; set; }

        public static List<Price> HistoryValues
        {
            get { return _historyVal; }
            set { Add(value); }
        }

        public static void Add(List<Price> newPrice)
        {
            //Save last 30 sec values in list like in queue
            if (_historyVal == null)
            {
                _historyVal = new List<Price>();
            }
            foreach (Price price in newPrice)
            {
                if (_historyVal.Count >= size)
                {
                    _historyVal.RemoveAt(0);
                }
                _historyVal.Add(price);
            }
        }
    }
}