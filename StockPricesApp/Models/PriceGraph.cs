﻿using System.Collections.Generic;

namespace StockPricesApp.Models
{
    public class PriceGraph
    {//Model for graph view
        public List<Price> Data;
        public PriceGraph()
        {
            Data=new List<Price>();
        }
    }
}