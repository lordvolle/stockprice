﻿using System;
using System.Linq;
using System.Threading;
using StockPricesApp.DAL;

namespace StockPricesApp.Models
{
    public class DbFiller
    {
//dbFiller will simulate work of some service with stock prices
        public static void Fill()
        {
            var stock = new stockEntities();

            //Start values
            double gold = 1234.56;
            double silver = 45.77;
            double palladium = 4571.26;
            var random = new Random(); //Random to generate new values and sleep time
            while (true)
            {
                double diff = (double) (random.Next(200) - 100)/100;
                double value = 0;
                string type = "";
                switch (random.Next(6)%3)
                {
                    case 0:
                        gold += diff;
                        value = gold;
                        type = "gold";
                        break;
                    case 1:
                        silver += diff;
                        value = silver;
                        type = "silver";
                        break;
                    case 2:
                        palladium += diff;
                        value = palladium;
                        type = "palladium";
                        break;
                }
                IQueryable<current_price> query = from elem in stock.current_price
                                                  where elem.type == type
                                                  select elem;
                current_price stockChanging = query.Single();
                stockChanging.time_stamp = DateTime.Now;
                stockChanging.value = (decimal) value;
                stock.SaveChanges();
                Thread.Sleep(random.Next(500) + 500);
            }
        }
    }
}