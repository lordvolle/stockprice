﻿using System;
using StockPricesApp.DAL;

namespace StockPricesApp.Models
{
    public class Price
    {
        public string Type;
        public decimal Value;
        public DateTime Time;

        public Price()
        {
        }

        public Price(string type, decimal value, DateTime time)
        {
            Type = type;
            Value = value;
            Time = time;
        }

        public void ConvertFromDb(current_price tmp)
        {
            Type = tmp.type;
            Value = tmp.value;
            Time = tmp.time_stamp;
        }
    }
}