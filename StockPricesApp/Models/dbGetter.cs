﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using StockPricesApp.DAL;

namespace StockPricesApp.Models
{
    public class DbGetter
    {
        public static void GetValues()
        {
            while (true)
            {
                var stock = new stockEntities();

                IQueryable<current_price> query = from elem in stock.current_price
                                                  select elem;
                List<current_price> elemList = query.ToList();
                var listCurrentPrices = new List<Price>();
                foreach (current_price currentPrice in elemList)
                {
                    var price = new Price();
                    price.ConvertFromDb(currentPrice);
                    listCurrentPrices.Add(price);
                }
                //Every 2 seconds get new values and fill global variables
                Global.CurrentValues = listCurrentPrices;
                Global.HistoryValues = listCurrentPrices;
                Thread.Sleep(2000);
            }
        }
    }
}