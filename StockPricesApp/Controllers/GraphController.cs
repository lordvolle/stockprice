﻿using System.Web.Mvc;
using StockPricesApp.Models;

namespace StockPricesApp.Controllers
{
    public class GraphController : Controller
    {

        public ActionResult Index(string type)
        {
            //Select data for graph from Global variables and put it in Model
            var graphData = new PriceGraph();
            foreach (Price prices in Global.HistoryValues)
            {
                if (prices.Type == type)
                {
                    graphData.Data.Add(prices);
                }
            }
            return View(graphData);
        }
    }
}