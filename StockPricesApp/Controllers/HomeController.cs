﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Web.Mvc;
using StockPricesApp.Models;

namespace StockPricesApp.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public void Message()
        {
            int steps = 10; //10 strings in message
            Response.ContentType = "text/event-stream";
            {
                while (steps > 0)
                {
                    steps--;

                    try
                    {
                        List<Price> currentValues = Global.CurrentValues;
                        string answer = "data: "; //Start of message
                        foreach (Price price in currentValues)
                        {
                            answer += string.Format("{0};{1};{2}|", price.Type, price.Value, price.Time);
                        }
                        answer += "\n\n\n"; //End of message
                        Response.Write(answer);
                        Response.Flush();
                    }
                    catch (Exception e)
                    {
                    }
                    Thread.Sleep(2000);
                }
            }
        }
    }
}