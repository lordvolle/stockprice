App to show stock prices in real time.

[Source code can be found by link](https://bitbucket.org/lordvolle/stockprice.git)
[Video on Youtube](https://youtu.be/5hibXd4nr8Y)
Requirements: 
Visual Studio 2010 with Asp.net and MVC support
MS SQL server
Browser: Google Chrome, Firefox

Installing:
1) Copy files from repository
2) Check connection string in Web.config
3) Run StockPriceApp.sln

Developed by Guzenko Oleg.
Used:
Chart library: free [Google Charts](https://developers.google.com/chart/)
CSS: [free template](https://templated.co/plaindisplay)
